import React from 'react';
import axios from 'axios';
// import Card from './Card/Card';
// import CardList from './Card/Card';
import './App.css';
import './Style.css';

// const axios = require('axios');

const CardList = (props) => (
  <div>
    {props.profiles.map((profile, index) => <Card key={index} {...profile} />)}
    {/* map method - variable to use later... second thing is the id for each element.  */}
  </div>
);

class Card extends React.Component {
  render() {
    const profile = this.props;
    return (
      <div className="github-profile">
        <img src={profile.avatar_url} alt="github" />
        <div className="info">
          <div className="name">Username: {profile.login}</div>
          <div className="name">Human name: {profile.name}</div>
          <div className="company">Company: {profile.company}</div>
          <div className="bio">Bio: {profile.bio}</div>
          <div className="bio">Public Repositories: {profile.public_repos}</div>
          <div className="bio">Location: {profile.location}</div>
          <div className="bio">User type: {profile.type}</div>
        </div>
      </div>
    );
  }
}

class Form extends React.Component {
  state = { userName: '' };
  handleSubmit = async (event) => {
    event.preventDefault();
    const response = await
      axios.get(`https://api.github.com/users/${this.state.userName}`);
    this.props.onSubmit(response.data);
    // console.log(this.state.userName);
    this.setState({ userName: '' })

  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text"
          value={this.state.userName}
          onChange={event => this.setState({ userName: event.target.value })}
          placeholder="Github username" required />
        <button>Add card</button>
      </form>
    );
  }
}

class App extends React.Component {

  state = {
    profiles: [],
  };

  addNewProfile = (profileData) => {
    // console.log('App', profileData);
    this.setState(prevState => ({
      profiles: [...prevState.profiles, profileData],
    }))
  }

  render() {
    return (
      <div>
        <div className="header">{this.props.title}</div>
        <Form onSubmit={this.addNewProfile} />
        <CardList profiles={this.state.profiles} />
      </div>
    );
  }
}

export default App;