# Application

I have been building a github cards application. It is quite simple, but has been very useful in terms of development.

I have taken knowledge from backend api calls and used that to pull extra data through, such as: 

1. Biography
2. Username
3. Public repos
4. Company name (if present)

I have enjoyed it so far and I have updated it on my #100daysofcode repo over on [github.](https://github.com/JRC404/100-days-of-code/blob/master/log.md)